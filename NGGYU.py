import time, random, sys, easygui, os
from threading import Thread
from threading import Timer

answer = None


def generate_question():
    num1 = random.randint(1, 101)
    num2 = random.randint(1, 101)
    question = "{} + {} = ?".format(str(num1), str(num2))
    return num1, num2, question


def check_time():
    time.sleep(5)
    if answer != None:
        return
    print("Too Slow")
    os.exec * ()


def hanan():
    try:
        while 1:
            easygui.msgbox('This is Hanan.', 'Hanan')
            time.sleep(0.5)
    except KeyboardInterrupt:
        num1, num2, question = generate_question()
        timeout = 5
        t = Timer(timeout, easygui.msgbox, ['Sorry, times up'])
        print(t)
        t.start()
        prompt = "You have %d seconds" % timeout
        answer = easygui.integerbox(question, prompt, upperbound=200)
        print(answer)
        if (answer == num1 + num2 and t.isAlive()):
            print("GJ!")
            t.cancel()
            sys.exit(0)
        elif answer != num1 + num2:
            print("Wrong")
            t.cancel()
            hanan()
        else:
            print(t)
            t.cancel()
            hanan()


if __name__ == '__main__':
    hanan()

